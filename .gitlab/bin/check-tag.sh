#!/bin/bash

set -e

is_version_tag() {
	if [ $# -lt 1 -o -z "$1" ]; then
		echo 'Usage: is_version_tag TAG' >&2
		return 1
	fi
	if [ $(grep -E '^v[0-9]+\.[0-9]+\.[0-9]+' <<< "$1" | wc -l) -lt 1 ]; then
		printf 'Not a version tag: %s\n' "$1" >&2
		return 1
	fi
	return 0
}

verbosity=0
exit_on_bad=0
while getopts ":ve" opt; do
	case ${opt} in
		v)
			verbosity=$((verbosity + 1))
			;;
		e)
			exit_on_bad=1
			;;
	esac
done
shift $((OPTIND - 1))

e_verbosity() {
	local opt OPTIND OPTARG
	local level=1
	while getopts ":l:" opt; do
		case ${opt} in
			l)
				level=$OPTARG
				;;
			\?)
				echo "Unknown argument: -$OPTARG" >&2
				return 1
				;;
			:)
				printf 'Invalid argument: -%s requires an argument\n' "$OPTARG" >&2
				return 1
				;;
		esac
	done
	shift $((OPTIND - 1))
	[ $verbosity -ge $level ] || return 0
	"$@"
}

if [ $# -lt 1 -o -z "$1" ]; then
	echo 'Usage: check-tag.sh TAG' >&2
	return 1
fi

for tag; do
	[ ! -z "$tag" ] || (echo "Invalid tag: \"$tag\"" >&2; exit 1)
	if is_version_tag "$tag"; then
		e_verbosity -l 1 printf '%s\n' "$tag"
	else
		[ $exit_on_bad -eq 0 ] || exit 1
	fi
done
