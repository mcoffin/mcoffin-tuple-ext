#!/bin/bash
if [ $# -lt 1 ]; then
	echo 'Usage: print_length.sh STR...' >&2
	exit 1
fi

get_length() {
	if [ $# -lt 1 ]; then
		wc -c
	else
		wc -c <<< "$1"
	fi
}

for arg; do
	printf '%d\n' $(get_length "$arg")
done
