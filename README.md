# mcoffin-tuple-ext

Simple extension trait for [`Option`](https://doc.rust-lang.org/std/option/enum.Option.html) for working with tupleized results.

# Example

```rust
extern crate mcoffin_tuple_ext;
use mcoffin_tuple_ext::OptionExt;

fn main() {
    let first = Some(1usize);
    assert_eq!(first.and_tup(first), Some((1usize, 1usize)));
    assert_eq!(first.and_tup(Some(2usize)), Some((1usize, 2usize)));
    assert_eq!(first.and_tup::<usize>(None), None);

    let v = first.and_then_tup(|_| Some("foo".to_string()));
    assert!(v.is_some());
    let (fst, snd) = v.unwrap();
    assert_eq!(fst, 1);
    assert_eq!(&snd, "foo");
}
```
