pub trait OptionExt<A>: Sized {
    /// If both `self` and the result of `f` are Some(..), then returns Some((a, b)), otherwise
    /// returns None
    fn and_then_tup<B, F>(self, f: F) -> Option<(A, B)> where
        F: FnOnce(&A) -> Option<B>;

    /// If both `self` and `v` are `Some(..)`, returns `Some(a, b)`, otherwise returns `None`
    #[inline]
    fn and_tup<B>(self, v: Option<B>) -> Option<(A, B)> {
        self.and_then_tup(move |_| v)
    }
}

impl<A: Sized> OptionExt<A> for Option<A> {
    fn and_then_tup<B, F>(self, f: F) -> Option<(A, B)> where
        F: FnOnce(&A) -> Option<B>,
    {
        self.and_then(|a| f(&a).map(move |b| (a, b)))
    }
}

#[cfg(test)]
mod tests;
