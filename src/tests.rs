use super::OptionExt;

const fn make_none<T: Sized>() -> Option<T> {
    None
}

#[test]
fn and_then_tup_works() {
    let first = Some(1usize);
    assert_eq!(first.and_then_tup(|&v| Some(v)), Some((1usize, 1usize)));
    assert_eq!(first.and_then_tup(|_| Some(2usize)), Some((1usize, 2usize)));
    assert_eq!(first.and_then_tup(|_| make_none::<usize>()), None);
}

#[test]
fn and_tup_works() {
    let first = Some(1usize);
    assert_eq!(first.and_tup(first), Some((1usize, 1usize)));
    assert_eq!(first.and_tup(Some(2usize)), Some((1usize, 2usize)));
    assert_eq!(first.and_tup::<usize>(None), None);
}

#[test]
fn make_none_first_results_in_make_none() {
    let first: Option<usize> = None;
    assert_eq!(first.and_then_tup(|_| Some(1usize)), None);
    assert_eq!(first.and_then_tup(|_| make_none::<usize>()), None);
    assert_eq!(first.and_tup(Some(1usize)), None);
    assert_eq!(first.and_tup::<usize>(None), None);
}

#[test]
fn it_works_with_strings() {
    let first = Some(1usize);
    let v = first.and_then_tup(|_| Some("foo".to_string()));
    assert!(first.is_some());
    let (fst, snd) = v.unwrap();
    assert_eq!(fst, 1);
    assert_eq!(&snd, "foo");
}
